//
//  TwitterFeedUITest.swift
//  TwitterClient
//
//  Created by Martin Chan on 06/04/2016.
//  Copyright © 2016 Macha Studios Ltd. All rights reserved.
//

import XCTest

class TwitterFeedUITest: XCTestCase {

    override func setUp() {
        super.setUp()

        continueAfterFailure = false
        let app = XCUIApplication()
        app.launchArguments = [TestHelper.uiTestKey, TwitterFeedDataSourceTests.Test20Results.rawValue]
        app.launch()
    }
    
    func testTimelineWith20Items() {
        let app = XCUIApplication()
        app.buttons["Log in with Twitter"].tap()

        let tablesQuery = XCUIApplication().tables
        tablesQuery.staticTexts["Zzz"].tap()
        tablesQuery.cells.containingType(.StaticText, identifier:"Martin").childrenMatchingType(.StaticText).matchingIdentifier("@machastudio").elementBoundByIndex(0).tap()
        
        tablesQuery.cells.elementBoundByIndex(UInt(0)).swipeUp()
        tablesQuery.cells.elementBoundByIndex(UInt(10)).swipeUp()
        tablesQuery.staticTexts["@EternaLEnVy1991 hey man you're late for practice"].tap()
    }
}
