//
//  TwitterClientUITests.swift
//  TwitterClientUITests
//
//  Created by Martin Chan on 05/04/2016.
//  Copyright © 2016 Macha Studios Ltd. All rights reserved.
//

import XCTest

class TwitterClientUITests: XCTestCase {

    override func setUp() {
        super.setUp()
        
        continueAfterFailure = false
        let app = XCUIApplication()
        app.launchArguments = [TestHelper.uiTestKey]
        app.launch()
    }

    func testLogin() {
        let app = XCUIApplication()
        app.buttons["Log in with Twitter"].tap()
        app.navigationBars["Feed"].staticTexts["Feed"].tap()
    }
}
