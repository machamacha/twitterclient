//
//  TwitterClientTests.swift
//  TwitterClientTests
//
//  Created by Martin Chan on 05/04/2016.
//  Copyright © 2016 Macha Studios Ltd. All rights reserved.
//

import XCTest

@testable import TwitterClient
import SwiftyJSON

class TwitterClientTests: XCTestCase {
    
    func testDateFormatter() {
        let testDateString = "Tue Aug 28 19:59:34 +0000 2012"
        guard let date = TwitterFeedDataItem.stringDateToNSDate(testDateString) else {
            XCTFail("couldn't get date from string")
            return
        }
        let calendar = NSCalendar.currentCalendar()
        calendar.timeZone = NSTimeZone(forSecondsFromGMT: 0)
        let datecomponents = calendar.components(NSCalendarUnit(rawValue: NSCalendarUnit.Month.rawValue | NSCalendarUnit.Year.rawValue | NSCalendarUnit.Weekday.rawValue | NSCalendarUnit.Hour.rawValue | NSCalendarUnit.Minute.rawValue | NSCalendarUnit.Second.rawValue | NSCalendarUnit.TimeZone.rawValue), fromDate: date)

        XCTAssertTrue(datecomponents.month == 8)
        XCTAssertTrue(datecomponents.hour == 19)
        XCTAssertTrue(datecomponents.minute == 59)
        XCTAssertTrue(datecomponents.second == 34)
        XCTAssertTrue(datecomponents.timeZone == NSTimeZone(forSecondsFromGMT: 0))
        XCTAssertTrue(datecomponents.year == 2012)
    }
    
    func testDateFormatter2() {
        let testDateString = "Sun Jul 08 01:05:04 +0500 1999"
        guard let date = TwitterFeedDataItem.stringDateToNSDate(testDateString) else {
            XCTFail("couldn't get date from string")
            return
        }
        let offset = 5 * 3600
        let calendar = NSCalendar.currentCalendar()
        calendar.firstWeekday = 1
        calendar.timeZone = NSTimeZone(forSecondsFromGMT: offset)
        let datecomponents = calendar.components(NSCalendarUnit(rawValue: NSCalendarUnit.Month.rawValue | NSCalendarUnit.Year.rawValue | NSCalendarUnit.Weekday.rawValue | NSCalendarUnit.Hour.rawValue | NSCalendarUnit.Minute.rawValue | NSCalendarUnit.Second.rawValue | NSCalendarUnit.TimeZone.rawValue), fromDate: date)
        

        XCTAssertTrue(datecomponents.month == 7)
        XCTAssertTrue(datecomponents.hour == 1)
        XCTAssertTrue(datecomponents.minute == 5)
        XCTAssertTrue(datecomponents.second == 4)
        XCTAssertTrue(datecomponents.timeZone == NSTimeZone(forSecondsFromGMT: offset))
        XCTAssertTrue(datecomponents.year == 1999)
    }
    
    func testDateStyleFormatter1() {
        let date = NSDate(timeIntervalSinceNow: -5)
        XCTAssertTrue(TwitterFeedDataItem.stringFormattedForDate(date) == "5s", "Should conform to short style 5s")
        let date2 = NSDate(timeIntervalSinceNow: 0)
        XCTAssertTrue(TwitterFeedDataItem.stringFormattedForDate(date2) == "0s", "Should conform to short style 0s")
        let date3 = NSDate(timeIntervalSinceNow: -60)
        XCTAssertTrue(TwitterFeedDataItem.stringFormattedForDate(date3) == "1m", "Should conform to short style 1m")
        let date4 = NSDate(timeIntervalSinceNow: -119)
        XCTAssertTrue(TwitterFeedDataItem.stringFormattedForDate(date4) == "1m", "Should conform to short style 1m")
        let date5 = NSDate(timeIntervalSinceNow: -60*60)
        XCTAssertTrue(TwitterFeedDataItem.stringFormattedForDate(date5) == "1h", "Should conform to short style 1h")
        let date6 = NSDate(timeIntervalSinceNow: -21*60*60)
        XCTAssertTrue(TwitterFeedDataItem.stringFormattedForDate(date6) == "21h", "Should conform to short style 21h")
        let date7 = NSDate(timeIntervalSinceNow: -24*60*60)
        XCTAssertTrue(TwitterFeedDataItem.stringFormattedForDate(date7) == "1d", "Should conform to short style 1d")
        let date8 = NSDate(timeIntervalSinceNow: -26*60*60)
        XCTAssertTrue(TwitterFeedDataItem.stringFormattedForDate(date8) == "1d", "Should conform to short style 1d")
        let date9 = NSDate(timeIntervalSinceNow: -2*24*60*60)
        XCTAssertTrue(TwitterFeedDataItem.stringFormattedForDate(date9) == "2d", "Should conform to short style 2d")
        let date10 = NSDate(timeIntervalSinceNow: -6*24*60*60)
        XCTAssertTrue(TwitterFeedDataItem.stringFormattedForDate(date10) == "6d", "Should conform to short style 6d")
        let date11 = NSDate(timeIntervalSinceNow: -6*24*60*60 - 1)
        XCTAssertTrue(TwitterFeedDataItem.stringFormattedForDate(date11) == "6d", "Should conform to short style 6d")
        let date12 = NSDate(timeIntervalSinceNow: -7*24*60*60 )
        XCTAssertTrue(TwitterFeedDataItem.stringFormattedForDate(date12).characters.count == 10 , "Should conform to long style")
        let date13 = NSDate(timeIntervalSinceNow: -31*24*60*60 )
        XCTAssertTrue(TwitterFeedDataItem.stringFormattedForDate(date13).characters.count == 10 , "Should conform to long style")
        let date14 = NSDate(timeIntervalSinceNow: -32*24*60*60 )
        XCTAssertTrue(TwitterFeedDataItem.stringFormattedForDate(date14).characters.count == 10 , "Should conform to long style")
        let date15 = NSDate(timeIntervalSinceNow: -365*24*60*60 )
        XCTAssertTrue(TwitterFeedDataItem.stringFormattedForDate(date15).characters.count == 10 , "Should conform to long style")
        let date16 = NSDate(timeIntervalSinceNow: -1000*24*60*60 )
        XCTAssertTrue(TwitterFeedDataItem.stringFormattedForDate(date16).characters.count == 10 , "Should conform to long style")

    }
    
    func testParseTimeLine1Item() {
        var item: TwitterFeedDataItem?
        guard let json = TestHelper.getJSONFromFile("MockTimelineItem", bundle: NSBundle(forClass: self.dynamicType)) else {
            XCTFail("Invalid file/file not json")
            return
        }
        item = TwitterFeedDataItem(json: json)

        XCTAssertNotNil(item, "parsing item failed. item must not be nil")
        XCTAssertTrue(item!.imageUrl.absoluteString == "http://a0.twimg.com/profile_images/730275945/oauth-dancer_normal.jpg")
        XCTAssertTrue(item!.author == "OAuth Dancer")
        XCTAssertTrue(item!.tweet == "just another test")
        XCTAssertTrue(item!.handle == "oauth_dancer")
    }
    
    func testParseTimeline2Array() {
        var arrayItems: [TwitterFeedDataItem]?
        guard let json = TestHelper.getJSONFromFile("MockTimeline", bundle: NSBundle(forClass: self.dynamicType)) else {
            XCTFail("Invalid file/file not json")
            return
        }
        XCTAssertNotNil(json.array, "json must be array")
        arrayItems = TwitterService.homeTimelineItemsFromJson(json)
        
        XCTAssertNotNil(arrayItems, "couldn't parse json file")
        XCTAssertTrue(json.array!.count == arrayItems!.count, "parsed TwitterFeedDataItem array count must equal json array count")
        XCTAssertTrue(arrayItems!.count > 0, "Item count must be greater than 0")
    }
    
    func testParseTimeline3ArrayEmpty() {
        var arrayItems: [TwitterFeedDataItem]?
        guard let json = TestHelper.getJSONFromFile("MockTimelineEmpty", bundle: NSBundle(forClass: self.dynamicType)) else {
            XCTFail("Invalid file/file not json")
            return
        }
        XCTAssertNotNil(json.array, "json must be array")
        arrayItems = TwitterService.homeTimelineItemsFromJson(json)
        
        XCTAssertNotNil(arrayItems, "couldn't parse json file")
        XCTAssertTrue(arrayItems!.count == 0, "Item count must be greater than 0")
    }
}
