//
//  TwitterFeedContentCell.swift
//  TwitterClient
//
//  Created by Martin Chan on 05/04/2016.
//  Copyright © 2016 Macha Studios Ltd. All rights reserved.
//

import UIKit
import Haneke

class TwitterFeedContentCell: UITableViewCell {
    static let reuseIdentifier = "TwitterFeedContentCell"
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var handleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var tweetLabel: UILabel!
    @IBOutlet weak var avatarImageView: UIImageView!
    
    func setupWithTwitterDataItem(item: TwitterFeedDataItem) {
        nameLabel.text = item.author
        handleLabel.text = "@\(item.handle)"
        dateLabel.text = item.dateStringFormatted
        tweetLabel.text = item.tweet
        avatarImageView.hnk_setImageFromURL(item.imageUrl)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        avatarImageView.hnk_cancelSetImage()
        avatarImageView.image = nil
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        avatarImageView.layer.cornerRadius = 5
    }
}
