//
//  TwitterFeedViewController.swift
//  TwitterClient
//
//  Created by Martin Chan on 05/04/2016.
//  Copyright © 2016 Macha Studios Ltd. All rights reserved.
//

import UIKit
import TwitterKit

class TwitterFeedViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    let dataSource = TwitterFeedDataSource()
    let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Feed"
        tableView.dataSource = dataSource
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 100
        tableView.addSubview(refreshControl)
        
        refreshControl.addTarget(self, action: #selector(TwitterFeedViewController.refreshControlChanged(_:)), forControlEvents: UIControlEvents.ValueChanged)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        if TestHelper.isUITest() {
            dataSource.updateDataItems([])
            tableView.reloadData()
        } else {
            refreshFeed()
        }
    }
    
    func refreshControlChanged(sender: UIRefreshControl) {
        refreshFeed()
    }
    
    func refreshFeed() {
        TwitterService.getHomeTimeline(success: { [weak self] (items) in
            self?.dataSource.updateDataItems(items)
            self?.tableView.reloadData()
            self?.refreshControl.endRefreshing()

            }, failure: { [weak self] error in
                self?.refreshControl.endRefreshing()
        })
    }
    
    @IBAction func pressedPost(sender: AnyObject) {
        guard let userID = Twitter.sharedInstance().sessionStore.session()?.userID else {
            return
        }

        let composer = TWTRComposerViewController(userID: userID)
        composer.delegate = self
        presentViewController(composer, animated: true, completion: nil)
    }
}

extension TwitterFeedViewController: TWTRComposerViewControllerDelegate {
    func composerDidSucceed(controller: TWTRComposerViewController, withTweet tweet: TWTRTweet) {
        refreshFeed()
    }
}
