//
//  ViewController.swift
//  TwitterClient
//
//  Created by Martin Chan on 05/04/2016.
//  Copyright © 2016 Macha Studios Ltd. All rights reserved.
//

import UIKit
import TwitterKit

class LoginViewController: UIViewController {

    @IBAction func pressedLogin(sender: AnyObject) {
        if TestHelper.isUITest() {
            onLoginComplete()
            return
        }

        Twitter.sharedInstance().logInWithMethods(.SystemAccounts) { [weak self] session, error in
            if (session != nil) {
                self?.onLoginComplete()
            } else {
                self?.loginWebased()
            }
        }
    }
    
    func loginWebased() {
        
        Twitter.sharedInstance().logInWithViewController(self, methods: .WebBased) { [weak self] session, error in
            if session != nil {
                self?.dismissViewControllerAnimated(true, completion: {
                    self?.onLoginComplete()
                })
            } else {
                print("error: \(error?.localizedDescription)")
            }
        }
    }
    
    func onLoginComplete() {
        performSegueWithIdentifier("toTwitterFeedSegue", sender: nil)
    }
}
