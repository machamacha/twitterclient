//
//  TwitterFeedDataSource.swift
//  TwitterClient
//
//  Created by Martin Chan on 05/04/2016.
//  Copyright © 2016 Macha Studios Ltd. All rights reserved.
//

import UIKit


class TwitterFeedDataSource: NSObject, UITableViewDataSource {
    
    private var dataItems = [TwitterFeedDataItem]()
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataItems.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(TwitterFeedContentCell.reuseIdentifier, forIndexPath: indexPath) as! TwitterFeedContentCell
        let item = dataItems[indexPath.row]
        cell.setupWithTwitterDataItem(item)
        return cell
    }
    
    func updateDataItems(items: [TwitterFeedDataItem]) {
        if TestHelper.isUITest() {
            dataItems = testDataForUITest()
        } else {
            dataItems = items
        }
    }
}

extension TwitterFeedDataSource {
    func testDataForUITest() -> [TwitterFeedDataItem] {
        // can cover multiple tests only cover 1 here
        if TestHelper.runningTest(TwitterFeedDataSourceTests.Test20Results.rawValue) {
            guard let json = TestHelper.getJSONFromFile("MockTimeline20", bundle: NSBundle.mainBundle()) else {
                return []
            }
            guard let items = TwitterService.homeTimelineItemsFromJson(json) else {
                return []
            }
            
            return items
        }
        return []
    }
}
