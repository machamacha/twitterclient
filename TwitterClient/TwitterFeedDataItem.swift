//
//  TwitterFeedDataItem.swift
//  TwitterClient
//
//  Created by Martin Chan on 05/04/2016.
//  Copyright © 2016 Macha Studios Ltd. All rights reserved.
//

import Foundation
import SwiftyJSON

struct TwitterFeedDataItem {
    let tweet: String
    let author: String
    let handle: String
    let imageUrl: NSURL
    let dateCreated: NSDate
    let dateStringFormatted: String
    
    init?(json: JSON) {
        if json.type != Type.Dictionary {
            return nil
        }
        guard let tweet = json["text"].string,
            author = json["user"]["name"].string,
            handle = json["user"]["screen_name"].string else {
                return nil
        }
        guard let imageString = json["user"]["profile_image_url"].string,
            imageUrl = NSURL(string: imageString) else {
                return nil
        }
        
        guard let dateString = json["created_at"].string,
            dateCreated = TwitterFeedDataItem.stringDateToNSDate(dateString) else {
            return nil
        }
        
        self.tweet = tweet
        self.imageUrl = imageUrl
        self.author = author
        self.handle = handle
        self.dateCreated = dateCreated
        self.dateStringFormatted = TwitterFeedDataItem.stringFormattedForDate(dateCreated)
    }
    
    /**
     Converts date into formatted in twitter style date since posted. If the difference is less than
     7 days, it shows short form, otherwise shows full date
     
     - Parameter date: date to convert
     - Returns: String formatted in correct style
     */
    static func stringFormattedForDate(date: NSDate) -> String {

        let calendar = NSCalendar.currentCalendar()
        let dateDiffComponents = calendar.components(NSCalendarUnit(rawValue: NSCalendarUnit.Year.rawValue | NSCalendarUnit.Hour.rawValue | NSCalendarUnit.Minute.rawValue | NSCalendarUnit.Second.rawValue | NSCalendarUnit.Month.rawValue | NSCalendarUnit.Day.rawValue), fromDate: date, toDate: NSDate(), options: NSCalendarOptions(rawValue:0))
        
        let dateDiff = (year: dateDiffComponents.year,
                        month: dateDiffComponents.month,
                        day: dateDiffComponents.day,
                        hour: dateDiffComponents.hour,
                        minute: dateDiffComponents.minute,
                        second: dateDiffComponents.second)

        switch dateDiff {
        // tweet older than 6 days, just show full date
        case let (year, month, day, _, _, _) where year > 0 || month > 0 || day > 6:
            let formatter = NSDateFormatter()
            formatter.dateFormat = "dd/MM/yyyy"
            return formatter.stringFromDate(date)
        // otherwise show short form date
        case let (_, _, day, _, _, _) where day > 0:
            return "\(day)d"
        case let (_, _, _, hour, _, _) where hour > 0:
            return "\(hour)h"
        case let (_, _, _, _, minute, _) where minute > 0:
            return "\(minute)m"
        default:
            return "\(dateDiffComponents.second)s"
        }
    }
    
    /**
        Converts twitter style created time to NSDate. If input is not in the correct format returns nil
        
        - Parameter string: input date string in format
        - Returns: NSDate. If input date invalid returns nils
    */
    static func stringDateToNSDate(string: String) -> NSDate? {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "ccc MMM d HH:mm:ss Z yyyy"
        
        return dateFormatter.dateFromString(string)
    }
}
