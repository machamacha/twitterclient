//
//  TwitterService.swift
//  TwitterClient
//
//  Created by Martin Chan on 06/04/2016.
//  Copyright © 2016 Macha Studios Ltd. All rights reserved.
//

import TwitterKit
import SwiftyJSON

class TwitterService {
    
    /**
     Get the latest timeline (home_timeline) of the current user. If the user isn't logged in it will fail
     
     - Parameters
        - count: number of feed items to fetch, default 20, range 1...200
        - success: success block containing an array of TwitterFeedDataItem (parsed feed items), called on main queue
        - failure: failure block, called on main queue
     */
    static func getHomeTimeline(count: Int = 20, success: (items: [TwitterFeedDataItem]) -> Void, failure: (NSError?) -> Void) {
        assert(count <= 200, "count cannot be greater than 200")
        assert(count > 0, "count should be greater than 0")

        let statusesShowEndpoint = "https://api.twitter.com/1.1/statuses/home_timeline.json"
        
        guard let userID = Twitter.sharedInstance().sessionStore.session()?.userID else {
            failure(nil)
            return
        }
        let client = TWTRAPIClient(userID: userID)

        let params = ["count": String(count)]
        var clientError: NSError?
        
        let request = client.URLRequestWithMethod("GET", URL: statusesShowEndpoint, parameters: params, error: &clientError)
        
        client.sendTwitterRequest(request) { (response, data, connectionError) -> Void in
            if connectionError == nil {
                guard let data = data else {
                    failure(clientError)
                    return
                }
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                    let json = JSON(data: data)
                    
                    guard let items = TwitterService.homeTimelineItemsFromJson(json) else {
                        dispatch_async(dispatch_get_main_queue(), {
                            failure(clientError)
                        })
                        return
                    }
                    dispatch_async(dispatch_get_main_queue(), {
                        success(items: items)
                    })
                })
            }
            else {
                print("Unhandled connection error: \(connectionError)")
                failure(connectionError)
            }
        }
    }
    
    /**
     Takes home_timeline json and returns array of TwitterFeedDataItems. 
     
     - Parameter json: home_timeline json. Must be an array
     */
    static func homeTimelineItemsFromJson(json: JSON) -> [TwitterFeedDataItem]? {
        assert(json.type == Type.Array)
        if let array = json.array {
            let items = array.flatMap { TwitterFeedDataItem(json: $0) }
            // flatMap will cause any parsing errors to be hidden (as TwitterFeedDataItem is failable),
            // so an assert for debugging is used to ensure the counts equal
            assert(items.count == array.count, "should be able to parse all json. If not check parser")
            return items
        }
        return nil
    }
}
