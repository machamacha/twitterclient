//
//  TestHelper.swift
//  TwitterClient
//
//  Created by Martin Chan on 06/04/2016.
//  Copyright © 2016 Macha Studios Ltd. All rights reserved.
//

import Foundation
import SwiftyJSON

enum TwitterFeedDataSourceTests: String {
    case Test20Results
}

class TestHelper {
    static let uiTestKey = "uiTestKey"

    static func getJSONFromFile(filename: String, bundle: NSBundle) -> JSON? {
        if let path = bundle.pathForResource(filename, ofType: "json") {
            do {
                let data = try NSData(contentsOfURL: NSURL(fileURLWithPath: path), options: NSDataReadingOptions.DataReadingMappedIfSafe)
                let jsonObj = JSON(data: data)
                
                return jsonObj != JSON.null ? jsonObj : nil
            } catch let error as NSError {
                print(error.localizedDescription)
                return nil
            }
        } else {
            print("no file found for: \(filename)")
            return nil
        }
    }
    
    static func isUITest() -> Bool {
        return runningTest(TestHelper.uiTestKey)
    }
    
    static func runningTest(testKey: String) -> Bool {
        return NSProcessInfo.processInfo().arguments.contains(testKey)
    }
}
